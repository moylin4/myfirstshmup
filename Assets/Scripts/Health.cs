﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Health : MonoBehaviour {

    public float MaxHealth = 3.0f;

    private float _curHealth = 3.0f;
    private bool _dead = false;

    public event EventHandler death;

    // Use this for initialization
    void Start()
    {
        _curHealth = MaxHealth;
        _dead = false;
    }

    public void TakeDamage(float damage)
    {
        _curHealth -= damage;

        if (_curHealth <= 0)
        {
            _curHealth = 0;
            if (!_dead && death != null)
            {
                death(this, EventArgs.Empty);
            }
            _dead = true;
            Destroy(this.gameObject);
        }
    }

    public float CurHealth
    {
        get { return _curHealth; }
    }

    public bool IsDead
    {
        get { return _dead; }
    }
}
