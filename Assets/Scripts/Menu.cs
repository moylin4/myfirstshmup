﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Menu : MonoBehaviour
{
    public string GameSceneName = "Game";
    
	// Use this for initialization
	void Start()
    {
	}

    public void LoadGameScene()
    {
        SceneManager.LoadScene(GameSceneName);
    }

    public void Quit()
    {
        Application.Quit();
    }
}
