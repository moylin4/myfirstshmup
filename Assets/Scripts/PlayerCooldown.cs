﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerCooldown : MonoBehaviour {

	public string AttackButton = "Fire2";
    public float AttackCooldown = 3.0f;
    public GameObject BulletPrefab = null;
    public AudioSource SfxSource = null;
    public AudioClip AttackSound = null;
    public float AttackVolume = 1.0f;

    private float _cooldown = 0;
    
    // Use this for initialization
    void Start()
    {
    }
    
    // Use this for initialization
	void Awake() 
    {
        StopAllCoroutines();
        StartCoroutine(AttackLoop());
	}

    void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawRay(transform.position, transform.up);
    }

    private IEnumerator AttackLoop()
    {
        for (;;)
        {
            // Wait for player to push button
            if (Input.GetButton(AttackButton))
            {
                // Spawn bullet
                Vector3 bulletPos = transform.position + transform.up;
                Quaternion bulletRot = Quaternion.identity;
                var bullet = GameObject.Instantiate(BulletPrefab, bulletPos, bulletRot);

                // Play sound effect
                if (SfxSource != null && AttackSound != null)
                {
                    SfxSource.PlayOneShot(AttackSound, AttackVolume);
                }

                // Wait for cooldown
                _cooldown = AttackCooldown;
                while (_cooldown > 0)
                {
                    yield return new WaitForSeconds(1.0f);
                    _cooldown -= 1.0f;
                }
            }
            else
            {
                // Wait for player to push button
                yield return new WaitForEndOfFrame();
            }
        }
    }
    
    public float Cooldown
    {
        get { return _cooldown; }
    }
}
