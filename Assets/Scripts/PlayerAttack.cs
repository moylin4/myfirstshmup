﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerAttack : MonoBehaviour 
{
    public string AttackButton = "Fire1";
    public float AttackRate = 2.0f; 
    public GameObject BulletPrefab = null;
    public AudioSource SfxSource = null;
    public AudioClip AttackSound = null;
    public float AttackVolume = 1.0f;

    // Use this for initialization
    void Start()
    {
    }
    
    // Use this for initialization6
	void Awake() 
    {
        StopAllCoroutines();
        StartCoroutine(AttackLoop());
	}

    void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawRay(transform.position, transform.up);
    }

    private IEnumerator AttackLoop()
    {
        for (;;)
        {
            // Wait for player to push button
            if (Input.GetButton(AttackButton))
            {
                // Spawn bullet
                Vector3 bulletPos = transform.position + transform.up;
                Quaternion bulletRot = transform.rotation;
                var bullet = GameObject.Instantiate(BulletPrefab, bulletPos, bulletRot);

                // Play sound effect
                if (SfxSource != null && AttackSound != null)
                {
                    SfxSource.PlayOneShot(AttackSound, AttackVolume);
                }

                // Wait for cooldown
                yield return new WaitForSeconds(1.0f / AttackRate);
            }
            else
            {
                // Wait for player to push button
                yield return new WaitForEndOfFrame();
            }
        }
    }
}
