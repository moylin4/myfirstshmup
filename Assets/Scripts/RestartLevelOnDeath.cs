﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class RestartLevelOnDeath : MonoBehaviour {

    public Health PlayerHealth;

	// Use this for initialization
	void Start()
    {
        PlayerHealth.death += PlayerHealth_death;
    }

    private void PlayerHealth_death(object sender, System.EventArgs e)
    {
        SceneManager.LoadScene(
            SceneManager.GetActiveScene().buildIndex);
    }
}
