﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyAttack : MonoBehaviour
{
    public float AttackRate = 1.0f;
    public GameObject BulletPrefab = null;

    // Use this for initialization
    void Start()
    {
    }

    // Use this for initialization
    void Awake()
    {
        StopAllCoroutines();
        StartCoroutine(AttackLoop());
    }

    void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawRay(transform.position, transform.up);
    }

    private IEnumerator AttackLoop()
    {
        for (;;)
        {
            Vector3 bulletPos = transform.position + transform.up;
            Quaternion bulletRot = transform.rotation;
            var bullet = GameObject.Instantiate(BulletPrefab, bulletPos, bulletRot);
            yield return new WaitForSeconds(1.0f / AttackRate);
        }
    }
}
