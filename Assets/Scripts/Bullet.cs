﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour 
{
    public float BulletSpeed = 15.0f;
    public float BulletLifeTime = 1.0f;
    public float BulletDamage = 1.0f;
    public string EnemyTag = "Enemy";
    
    private Rigidbody2D _rigidBody = null;

	// Use this for initialization
    void Start()
    {
        _rigidBody = GetComponent<Rigidbody2D>();
        _rigidBody.velocity = transform.up * BulletSpeed;
        Destroy(this.gameObject, BulletLifeTime);
    }
    
    void OnCollisionEnter2D(Collision2D col)
    {
        string tag = col.gameObject.tag;
        if (tag == EnemyTag)
        {
            col.gameObject.SendMessage("TakeDamage", BulletDamage);
            Destroy(this.gameObject);
        }
    }
}
