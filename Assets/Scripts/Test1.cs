﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Test1 : MonoBehaviour
{
    public string Name = "Hello World";
    public int Level = 3;

	// Use this for initialization
	void Start()
    {
        Debug.LogFormat("BANANA {0}: {1}", Name, Level);
	}
	
	// Update is called once per frame
	void Update()
    {
        Debug.LogFormat("STRAWBERRY {0}: {1}", Name, Level);
    }
}
