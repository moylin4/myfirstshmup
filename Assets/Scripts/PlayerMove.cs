﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMove : MonoBehaviour
{
    public string HorizontalAxis = "Horizontal";
    public string VerticalAxis = "Vertical";
    public float MaxHorizontalSpeed = 10.0f;
    public float MaxVerticalSpeed = 10.0f;

    private Vector2 _inputVelocity = Vector2.zero;
    private Rigidbody2D _rigidBody = null;
    
    // Use this for initialization
    void Start()
    {
        _inputVelocity = Vector2.zero;
        _rigidBody = GetComponent<Rigidbody2D>();
    }
	
	// Update is called once per frame
	void Update()
    {
        float h = Input.GetAxis(HorizontalAxis);
        float v = Input.GetAxis(VerticalAxis);
        _inputVelocity = new Vector2(
            h * MaxHorizontalSpeed, v * MaxVerticalSpeed);
    }

    // FixedUpdate is called once per physics step
    void FixedUpdate()
    {
        _rigidBody.velocity = _inputVelocity;
    }
}
