﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(AudioSource))]
public class Music : MonoBehaviour
{
    private static Music _instance;
    public AudioSource _audio;

    public static Music Instance
    {
        get
        {
            if (_instance == null)
            {
                _instance = GameObject.FindObjectOfType<Music>();
                if (_instance != null)
                {
                    _instance.UpdateInstance();
                }
            }
            return _instance;
        }
    }

    public AudioSource Audio
    {
        get { return _audio; }
    }

    private void UpdateInstance()
    {
        _audio = GetComponent<AudioSource>();

        if (_instance == this)
        {
            // Don't destroy me
        }
        else if (_instance == null)
        {
            // Don't destroy me
            DontDestroyOnLoad(gameObject);
            _instance = this;
        }
        else
        {
            // Destroy me
            Destroy(gameObject);
        }
    }
    
    void Start()
    {
        UpdateInstance();
    }
	
	void Awake()
    {
        UpdateInstance();
    }
}
