﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyMove : MonoBehaviour {
    
    public float HorizontalSpeed = 3.0f;
    public float HorizontalPeriod = 2.0f;
    public float VerticalSpeed = 1.0f;

    private const float TAU = Mathf.PI * 2.0f;
    private float _theta = 0;
    private Rigidbody2D _rigidBody = null;

    // Use this for initialization
    void Start()
    {
        _theta = Random.Range(0, TAU);
        _rigidBody = GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update()
    {
    }

    // FixedUpdate is called once per physics step
    void FixedUpdate()
    {
        _theta += (TAU / HorizontalPeriod) * Time.deltaTime;

        float h = Mathf.Sin(_theta);
        float v = 1;
        _rigidBody.velocity =
            transform.right * h * HorizontalSpeed +
            transform.up * v * VerticalSpeed;
    }
}
